const config = require("../config.json");
const RconClient = require("./RconClient.js");
const DateHelper = require("./DateHelper.js");
const dh = new DateHelper();
const { exec } = require('child_process');
const fetch = require('node-fetch');
const io = require("./io.js");

module.exports = class AutoRestart {	
	constructor(client, debugTag) {
		this.debugTag = debugTag || "";
		
		this.client = client;
		this.channel = client.guilds.cache.find((e) => {return e.name===config.ServerName;}).channels.cache.find((e) => {return e.name===config.ServerStatusChannel;})
		
		if(!io.exists(config.ModStatusFilePath))
		{
			io.writeFile(config.ModStatusFilePath, {});
		}
		this.modList = io.readFile(config.ModStatusFilePath);
		
		this.timeoutOne = null;
		this.timeoutTwo = null;
		
		this.rc = new RconClient(config.RconIp, config.RconPort, config.RconPassword, "AutoRestart");
		this.rc.connect();
		
		setInterval(()=>this.checkForModUpdates(), 60000);
	}
	
	restartServer() {
		if(this.timeoutOne==null) {
			this.log("Auto-Magical™ restart for mod update in 5 minutes");
			this.channel.send("```Auto-Magical™ restart for mod update in 5 minutes```");
			this.rc.send("broadcast Auto-Magical™ restart for mod update in 5 minutes");
			
			this.timeoutOne = setTimeout(() => {
				this.log("Auto-Magical™ restart for mod update in 1 minute");
				this.channel.send("```Auto-Magical™ restart for mod update in 1 minute```");
				this.rc.send("broadcast Auto-Magical™ restart for mod update in 1 minute");
			}, 240000);
			
			this.timeoutTwo = setTimeout(() => {
				this.log("Auto-Magical™ restart for mod update NOW");
				this.channel.send("```Auto-Magical™ restart for mod update NOW```");
				exec("taskkill /F /IM  ConanSandboxServer-Win64-Test.exe");
				this.timeoutOne = null;
				this.timeoutTwo = null;
			}, 300000);
		} else {
			this.log("A restart is already in progress");
		}
	}
	
	checkForChanges(mods) {
		this.log("Checking if mod list changed ...");
		if(Object.getOwnPropertyNames(this.modList).length===Object.getOwnPropertyNames(mods).length) {
			this.log("Checking mods for updates ...");
			for(var key in mods) {
				if(mods[key]!=this.modList[key]) {
					this.modList = mods;
					io.writeFile(config.ModStatusFilePath, this.modList);
					return true;
				}
			}
		} else {
			this.log("Mod list changed. Writing mod list to file ...")
			this.modList = mods;
			io.writeFile(config.ModStatusFilePath, this.modList);
		}
		
		this.log("No mod updates found");
		return false;
	}
	
	checkForModUpdates() {
		this.log("Checking for mod updates ...");
		this.log("Getting list of mods from config ...");
		var modIds = config.Mods.replace(/ /gi, "").split(",");
		var modIdsLength = modIds.length;
		var modIdsStr = modIds.reduce((a, c, i) => a==="" ? a=`publishedfileids[${i}]=${c}` : a+=`&publishedfileids[${i}]=${c}`, "");
		var body = `itemcount=${modIdsLength}&${modIdsStr}`;

		this.log("Querying steam ...");
		fetch("https://api.steampowered.com/ISteamRemoteStorage/GetPublishedFileDetails/v1/", {
			method: "POST",
			headers: {
				"Content-Type": "application/x-www-form-urlencoded"
			},
			body: body
		})
		.then(res => res.json())
		.then(json => json.response.publishedfiledetails.reduce((a, c) => (a[c.publishedfileid]=c.time_updated, a), {}))
		.then(mods => {
			for(var key in mods) {
				this.log(`ModID: ${key} LastUpdated: ${dh.unixToDateTime(mods[key])}`);
			}
			
			if(this.checkForChanges(mods)) {
				
				this.restartServer();
			}
		})
		.catch(err => this.log(err));
	}
	
	log(text) {
		console.log(`${dh.getDateTimeSec()} ${this.debugTag}: ${text}`);
	}
}