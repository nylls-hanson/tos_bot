const config = require("../config.json");
const RconClient = require("./RconClient.js");
const { exec } = require('child_process');

module.exports = class DiscordRestart {	
	constructor() {
		this.timeoutOne = null;
		this.timeoutTwo = null;
		
		this.rc = new RconClient(config.RconIp, config.RconPort, config.RconPassword, "DiscordRestart");
		this.rc.connect();
	}
	
	processMessage(msg) {
		if(!msg.author.bot) {
			msg.react("✅");
			if(msg.content.toUpperCase()==="RESTART") {
				if(this.timeoutOne==null) {
					msg.channel.send("```Server restarting in 5 minutes```");
					this.rc.send("broadcast Server restarting in 5 minutes");
					
					this.timeoutOne = setTimeout(() => {
						msg.channel.send("```Server restarting in 1 minute```");
						this.rc.send("broadcast Server restarting in 1 minute");
					}, 240000);
					
					this.timeoutTwo = setTimeout(() => {
						msg.channel.send("```Server restarting NOW```");
						exec("taskkill /F /IM  ConanSandboxServer-Win64-Test.exe");
						this.timeoutOne = null;
						this.timeoutTwo = null;
					}, 300000);
				} else {
					msg.channel.send("```A restart is already in progress.```");
				}
			} else if(msg.content.toUpperCase()==="RESTART CANCEL") {
				clearTimeout(this.timeoutOne);
				clearTimeout(this.timeoutTwo);
				this.timeoutOne = null;
				this.timeoutTwo = null;
				msg.channel.send("```Server restart canceled```");
				this.rc.send("broadcast Server restart canceled");
			} else {
				msg.channel.send("```Invalid command. Please try using \"restart\" or \"restart cancel\".```");
			}
		}
	}
}