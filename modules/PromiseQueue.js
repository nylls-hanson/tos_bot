module.exports = class PromiseQueue {
	constructor() {
		this.queue = Promise.resolve();
	}
	
	addToQueue(jobFunction) {
		this.queue.then(jobFunction);
	}
}