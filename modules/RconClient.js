var net = require("net");
var PromiseQueue = require("./PromiseQueue.js");
const DateHelper = require("./DateHelper.js");
const dh = new DateHelper();

module.exports = class RconClient {	
	
	constructor(ip, port, password, debugTag) {
		this.ip = ip;
		this.port = port;
		this.password = password;
		this.authenticated = false;
		this.id = 0;
		this.debugTag = debugTag || "";
		
		this.pq = new PromiseQueue();
	}
	
	reconnect() {
		this.authenticated = false;
		this.conn.removeAllListeners();
		this.conn.destroy();
		this.log("Reconnecting in 3 seconds");
		setTimeout(()=>this.connect(), 3000);
	}
	
	connect() {
		this.log("Initiating connection ...");
		
		this.conn = new net.Socket();
		
		this.conn.on("connect", () => {
			this.conn.removeAllListeners("connect");
			this.log("Authenticating ...");
			this.conn.write(this.createRequest(3, this.password));
		});
		
		this.conn.on("data", (data) => {
			this.conn.removeAllListeners("data");
			var res = this.readResponse(data).body;
			this.log(res);
			if(res==="Authenticated.") {
				this.authenticated = true;
				this.conn.setKeepAlive(true);
				this.conn.setNoDelay(true);
				setInterval(()=>this.pq.addToQueue(()=>{
					if(this.authenticated) {
						this.log("Pinging RCON server ...")
						this.send("TosTest Ping");
					}
				}), (Math.floor(Math.random()*180000)+60000));
			} else {
				this.log(this.readResponse(data).body);
				this.log("Failed to authenticate ... reconnecting in 60 seconds");
				setTimeout(()=>this.connect(), 60000);
			}
		});
		
		this.conn.on("error", () => {
			this.log("Connection error");
		});
		
		this.conn.on("close", () => {
			this.log("Connection closed");
			this.reconnect();
		});
		
		this.conn.on("end", () => {
			this.log("Connection ended");
			this.reconnect();
		});
		
		this.conn.connect(this.port, this.ip);
	}
	
	close() {
		this.conn.end();
		this.conn.destroy();
	}
	
	send(command, callback) {
		this.pq.addToQueue(() => {
			this.conn.on("data", (data) => {
				this.conn.removeAllListeners("data");
				this.log("Receiving response ...");
				var res = this.readResponse(data).body;
				this.log(res);
				
				if(callback) {
					callback(res);
				}
			});
			
			this.log("Sending command ...");
			this.conn.write(this.createRequest(2, command));
			this.log("Sent command!");
		});
	}
	
	createRequest(type, body) {
		var size   = Buffer.byteLength(body) + 14,
			buffer = new Buffer.alloc(size);

		buffer.writeInt32LE(size - 4, 0);
		buffer.writeInt32LE(this.id,       4);
		buffer.writeInt32LE(type,     8);
		buffer.write(body, 12, size - 2, "ascii");
		buffer.writeInt16LE(0, size - 2);

		this.id++;
		
		return buffer;
	}

	readResponse(buffer) {
		var response = {
			size: buffer.readInt32LE(0),
			id:   buffer.readInt32LE(4),
			type: buffer.readInt32LE(8),
			body: buffer.toString("ascii", 12, buffer.length - 2)
		}

		return response;
	}
	
	log(text) {
		//console.log(`${dh.getDateTimeSec()} ${this.debugTag}: ${text}`);
	}
}