const config = require("../config.json");

module.exports = class DiscordEmoji {	
	constructor() {}
	
	processMessage(msg) {
		if(!msg.author.bot) {
			var emojiName = msg.content.substring(7);
			if(emojiName.includes("<") && emojiName.includes(":") && emojiName.includes(">")) {
				emojiName = emojiName.substring(emojiName.indexOf(":")+1, emojiName.indexOf(":", emojiName.indexOf(":")+1));
			} else if([...emojiName].length===1) {
				emojiName = "\\" + emojiName;
			} else {
				emojiName = "Invalid emoji. Please pass a single emoji as a parameter.";
			}
			msg.channel.send(`${emojiName}`);
		}
	}
}