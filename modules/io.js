const fs = require("fs");

module.exports = class AutoRestart {	
	static exists(file) {
		return fs.existsSync(file);
	}

	static writeFile(fileName, data) {
		var data = JSON.stringify(data, null, "\t");  
		fs.writeFileSync(fileName, data); 
	}

	static readFile(fileName) {
		return JSON.parse(fs.readFileSync(fileName));
	}
}