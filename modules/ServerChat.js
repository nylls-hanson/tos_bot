const config = require("../config.json");
const RconClient = require("./RconClient.js");

module.exports = class ServerChat {	
	constructor() {
		this.rc = new RconClient(config.RconIp, config.RconPort, config.RconPassword, "ServerChat");
		this.rc.connect();
	}
	
	processMessage(msg) {
		if(!msg.author.bot) {
			this.rc.send(`server (${msg.author.username}) ${msg.content}`);
			msg.react("✅");
		}
	}
}