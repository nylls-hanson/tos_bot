var config = null;
const io = require("./io.js");

module.exports = class DiscordRole {	
	constructor(client) {
		config = io.readFile("config.json");
		this.initialize(client);
	}
	
	async initialize(client) {
		if(config.RoleMsgId==="") {
			var channel = client.guilds.cache.find((e) => {return e.name===config.ServerName;}).channels.cache.find((e) => {return e.name===config.RoleMsgChannel;});
			
			var msg = await channel.send(config.RoleMsgTxt);
			
			var roles = config.RoleAssignment;
			for(var key in roles) {
				await msg.react(key);
			}
			
			config.RoleMsgId = msg.id;
			io.writeFile("config.json", config);
		}
	}
	
	async processReaction(reaction, user, action) {
		if(!user.bot) {
			if (reaction.partial) {
				try {
					await reaction.fetch();
				} catch (error) {
					console.log('Something went wrong when fetching the message: ', error);
					return;
				}
			}
			
			var message = reaction.message;
			
			if(message.id===config.RoleMsgId) {
				var emojiName = reaction.emoji.name;
				var guild = message.guild;
				var member = await guild.members.fetch(user.id);
				
				if(config.RoleAssignment.hasOwnProperty(emojiName)) {
					var role = guild.roles.cache.find(r => r.name === config.RoleAssignment[emojiName]);
					
					if(action==="add") {
						await member.roles.add(role);
					} else if(action==="remove") {
						await member.roles.remove(role);
					}
				} else {
					await reaction.remove();
				}
			}
		}
	}
}