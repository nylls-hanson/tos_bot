require("regenerator-runtime/runtime");
const Discord = require("discord.js");
const client = new Discord.Client({ partials: ['MESSAGE', 'CHANNEL', 'REACTION'] });
const config = require("./config.json");
const DateHelper = require("./modules/DateHelper.js");
const dh = new DateHelper();
const ServerChat = require("./modules/ServerChat.js");
const sc = new ServerChat();
const DiscordRcon = require("./modules/DiscordRcon.js");
const dr = new DiscordRcon();
const DiscordRestart = require("./modules/DiscordRestart.js");
const drestart = new DiscordRestart();
const AutoRestart = require("./modules/AutoRestart.js");
var arestart = null;
const DiscordEmoji = require("./modules/DiscordEmoji.js");
const de = new DiscordEmoji();
const DiscordRole = require("./modules/DiscordRole.js");
var drl = null;

client.on("ready", () => {
	console.log(`${dh.getDateTimeSec()} ToS Bot: Logged in as ${client.user.tag}!`);
	arestart = new AutoRestart(client, "AutoRestart");
	drl = new DiscordRole(client);
});

client.on("message", msg => {
	if(msg.type==="DEFAULT") {
		if(msg.content.substring(0,6).toLowerCase()==="$emoji") {
			de.processMessage(msg);
		} else if(msg.channel.name===config.ServerChatChannel) {
			sc.processMessage(msg);
		} else if(msg.channel.name===config.RconChannel) {
			dr.processMessage(msg);
		} else if(msg.channel.name===config.RestartChannel) {
			drestart.processMessage(msg);
		}
	}
});

client.on('messageReactionAdd', async (reaction, user) => {
	await drl.processReaction(reaction, user, "add");
});

client.on('messageReactionRemove', async (reaction, user) => {
	await drl.processReaction(reaction, user, "remove");
});

client.login(config.DiscordApiToken);